# Learnicious

Learnicious is a academic-purposes oriented project. I do not want to make profit of it. I just want it to learn more about Django, React and Django REST Framework. 

## Project structure
- `learnicious`: Django backend and API. 
- `learnface`: React SPA (API Client)

## Contributors
- Franco Correa <franco@francocorrea.com.uy>
- You? [Send a Merge Request](https://gitlab.com/francocorreasosa/reactive-dj-lms/merge_requests/new)

## Tools used
- IDE: Visual Studio Code
- Text Editor (`.md` and others): VIM
- Database: PostgreSQL
