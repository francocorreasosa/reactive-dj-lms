import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import Home from './routes/Home';
import { Router, Route, browserHistory } from 'react-router';

const router = <Router history={browserHistory}>
  <Route path="/" component={Home} />
</Router>

ReactDOM.render(
  router,
  document.getElementById('root')
);
