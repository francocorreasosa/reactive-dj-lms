import React, { Component } from 'react';
import '../App.scss';

import Header from 'grommet/components/Header';
import Title from 'grommet/components/Title';
import Box from 'grommet/components/Box';
import Menu from 'grommet/components/Menu';
import Anchor from 'grommet/components/Anchor';
import Search from 'grommet/components/Search';
import Actions from 'grommet/components/icons/base/Actions';

class Home extends Component {
  render() {
    return (
      <div className="Home">
        <Header>
          <Title>
            Sample Title
          </Title>
          <Box flex={true}
            justify="end"
            direction="row"
            responsive={false}>
            <Search inline={true}
              fill={true}
              size="medium"
              placeHolder="Search"
              dropAlign={{"right": "right"}} />
            <Menu icon={<Actions />}
              dropAlign={{"right": "right"}}>
              <Anchor href="#"
                className="active">
                First
              </Anchor>
              <Anchor href="#">
                Second
              </Anchor>
              <Anchor href="#">
                Third
              </Anchor>
            </Menu>
          </Box>
        </Header>
      </div>
    );
  }
}

export default Home;
