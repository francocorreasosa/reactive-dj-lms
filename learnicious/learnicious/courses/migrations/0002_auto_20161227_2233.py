# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-28 01:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='is_public',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='courseclass',
            name='is_public',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='course',
            name='background_image',
            field=models.ImageField(upload_to='course_images/'),
        ),
    ]
