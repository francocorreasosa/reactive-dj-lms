from django.conf.urls import url, include
from rest_framework import routers
import courses.viewsets as viewsets

router = routers.DefaultRouter()
router.register('courses', viewsets.CourseViewSet)
router.register('classes', viewsets.CourseClassViewSet)
router.register('teachers', viewsets.TeacherViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
]
