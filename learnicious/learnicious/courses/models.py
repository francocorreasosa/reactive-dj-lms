from django.db import models


class Teacher(models.Model):

    name = models.CharField(max_length=255)
    twitter_handle = models.CharField(max_length=100)
    description = models.CharField(max_length=255) # Brief description

    def __str__(self):
        return self.name


class CourseClass(models.Model):

    title = models.CharField(max_length=255)
    content = models.TextField() # Markdown content (if)
    video_id = models.CharField(max_length=255, null=True, blank=True)
    is_public = models.BooleanField(default=True)

    def __str__(self):
        return self.title


class Course(models.Model):

    title = models.CharField(max_length=255)
    headline = models.CharField(max_length=500)
    description = models.TextField()
    background_image = models.ImageField(upload_to="course_images/")
    teachers = models.ManyToManyField("Teacher")
    classes = models.ManyToManyField("CourseClass")
    is_public = models.BooleanField(default=True)

    def __str__(self):
        return self.title

