from rest_framework import serializers
from .models import Course, CourseClass, Teacher


class CourseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Course
        fields = ['id', 'title', 'headline', 'description', 'background_image']


class CourseClassSerializer(serializers.ModelSerializer):

    class Meta:
        model = CourseClass
        fields = ['id', 'title', 'content', 'video_id']


class TeacherSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Teacher
        fields = ['id', 'name', 'twitter_handle', 'description']